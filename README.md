# JuliaBoxTutorials

Cross-platform fork of the popular tutorials for learning Julia. Original hosted on GitHub at https://github.com/JuliaComputing/JuliaBoxTutorials
Original credits go to them, as per the commit messages (obviously)